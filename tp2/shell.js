var fs = require("fs");
var http = require("http");
process.stdin.setEncoding('utf8');

function processLs(paths) {
	if (paths.length === 0) {
		paths.push(".");
	} 
	console.log("paths = ", paths);
	paths.forEach( function( path ) {        
		console.log("path = ", path);
		fs.readdir( path, function(err, files) {
			files.forEach( file => console.log(file) )
		})
	});
}

function processPwd( args ) {
	console.log( process.cwd() );
}

function processWget( args ) {
	if (args.length === 0) {
		console.log("wget requires at least an argument");
	} else {
		args.forEach(
				url => {
					var basename = url.split("/").slice(-1)[0];
					console.log("writing in ", basename);
					var file = fs.createWriteStream(basename);
					http.get( url, function(res) {
						res.pipe(file);
					})
				}
			    )
	}
}


process.stdin.on('readable', function() {
	var chunk = process.stdin.read();
	if (chunk !== null) {
		chunk = chunk.trim(); // to get rid of "/n" characters
		var parts = chunk.split(" ");
		var command = parts[0];
		var args = parts.splice(1);
		if (/ls/.exec(parts[0]) !== null) {
			processLs( args ); 
		} else if (/pwd/.exec(parts[0]) !== null) {
			processPwd( args );
		} else if (/wget/.exec(parts[0])) { 
			processWget( args );
		}
	}
});
process.stdin.on('end', function() {
	process.stdout.write('end');
});
