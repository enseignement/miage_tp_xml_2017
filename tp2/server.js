var http = require('http');
var fs = require('fs');
var url = require('url');
var qs = require('querystring');

var server = http.createServer(function (req, res) {
	var file = 'infos.html';
	res.statusCode = 200;
	var params = qs.parse(url.parse(req.url).query);
	var val = {
		name : params['name'],
		node : process.versions.node,
		v8 : process.versions.v8,
		url : req.url,
		time : new Date()
	}
	fs.readFile(file, function (err, data) {
		if (err) {
			res.write('Error ' + e);
		} else {
			console.log(req.url);
			var html = data.toString();
			// placeholders
			var ph = html.match(/\{\w+\}/g);
			for (p in ph) {
				console.log(ph[p]);
				console.log(val[ph[p].replace(/[\{\}]/g, '')]);
				var html = html.replace(ph[p], val[ph[p].replace(/[\{\}]/g, '')]);
			}
			res.write(html);
		}
		res.end();
	});
});

server.listen(1337);
