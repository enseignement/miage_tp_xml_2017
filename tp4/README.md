# Rôle des fichers :

## Fichiers pour la validation par dtd
* zoo.dtd
* zoo.dtd.xml
* notations.ent

## Exemple de validation par xsd 
* personne.xml
* personne.xsd

## Version du zoo avec validation xsd
* zoo.xsd
* zoo.xsd.ok.xml
* zoo.xsd.xml

## Source du validateur pour xsd
* parser/src/fr/miage1/tpwebxml/Main.java

# Pour valider avec le programme java

* Compiler
`javac parser/src/fr/miage1/tpwebxml/Main.java`
* Exécuter
`java -cp ./parser/out/production/parser fr.miage1.tpwebxml.Main personne.xml personne.xsd `
