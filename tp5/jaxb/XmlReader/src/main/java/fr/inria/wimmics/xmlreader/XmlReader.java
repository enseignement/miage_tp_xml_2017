/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.inria.wimmics.xmlreader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.sax.SAXSource;
import org.inria.ns.tp.jaxb.zoo.Animal;
import org.inria.ns.tp.jaxb.zoo.VisiteMédicale;
import org.inria.ns.tp.jaxb.zoo.Zoo;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

/**
 *
 * @author edemairy
 */
public class XmlReader {

	public static class DateAdapter extends XmlAdapter<XMLGregorianCalendar, LocalDate> {
		public void DateAdapter(){}
		@Override
		public LocalDate unmarshal(XMLGregorianCalendar xmlDate) throws Exception {
			Date utilDate = xmlDate.toGregorianCalendar().getTime();
			return LocalDateTime.ofInstant(utilDate.toInstant(), ZoneId.systemDefault()).toLocalDate();
		}

		@Override
		public XMLGregorianCalendar marshal(LocalDate date) throws Exception {
			Date utilDate = Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
			GregorianCalendar cal = new GregorianCalendar();
			cal.setTime(utilDate);
			return DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
		}

	}

	public static void main(String[] args) throws JAXBException, ParserConfigurationException, SAXException, FileNotFoundException {
		// pour convertir le document XML en objets :
		JAXBContext jaxb = JAXBContext.newInstance(Zoo.class);
		Unmarshaller unmarshaller = jaxb.createUnmarshaller();

		SAXParserFactory spf = SAXParserFactory.newInstance();

		spf.setXIncludeAware(true);
		spf.setNamespaceAware(true);
		spf.setValidating(true); // Not required for JAXB/XInclude

		XMLReader xr = spf.newSAXParser().getXMLReader();
		SAXSource source = new SAXSource(xr, new InputSource(new FileInputStream(args[0])));

		Zoo zoo = (Zoo) unmarshaller.unmarshal(source);

		// pour obtenir une date en Java 8 :
		LocalDate date = LocalDate.parse("2000-01-01");

		for (Animal animal : zoo.getAnimal()) {
			System.out.println(animal.getNom());
			if (animal.getDateNaissance().isAfter(date)) {
				System.out.println(animal.getDateNaissance());
				animal.setVisiteMédicale(VisiteMédicale.À_FAIRE);
			}
		}

		// pour convertir les objets en document XML :
		Marshaller marshaller = jaxb.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		marshaller.marshal(zoo, new File(args[1]));

	}
}
