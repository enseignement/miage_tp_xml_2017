package org.inria.ns.tp.jaxb.zoo;

import java.time.LocalDate;
import javax.xml.bind.JAXBElement;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;

public class AnimalAdapter extends XmlAdapter<JAXBElement<? extends Animal>, Animal> {

	@SuppressWarnings("unchecked")
	@Override
	public JAXBElement<Animal> marshal(Animal v) throws Exception {
		QName name = new QName(v.getClass().getSimpleName().toLowerCase());
		return new JAXBElement<Animal>(name, (Class<Animal>) v.getClass(), v);
	}

	@Override
	public Animal unmarshal(JAXBElement<? extends Animal> v) throws Exception {
		return v.getValue();
	}
}
